package ss.bpm.test;

import org.jbpm.services.api.ProcessService;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.internal.process.CorrelationKey;

public class RoutingWorkItemHandler implements WorkItemHandler {

	private static final String ROUTING_EXCEPTION = "ROUTING_EXCEPTION";
	
	private ProcessService processService; 
	
	private void sanityCheck(ProcessService ps, String id) {
		
		if( id == null || id.isEmpty() )
			throw new IllegalStateException("[RoutingWorkItemHandler] ID is missing ... ");
		
		if( ps == null || id.isEmpty() )
			throw new IllegalStateException("[RoutingWorkItemHandler] Unable to retrieve Process Service ... ");
	}
	
	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		// TODO Auto-generated method stub
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		processService = SpringBridge.services().getProcessService();
		
		// The id of the process where the signal should be sent
		String id = (String) workItem.getParameter("ID");
		
		sanityCheck(processService, id);
		
		boolean throwEexception = workItem.getParameter("THROW") != null;
		
		if( throwEexception ) {
			// Signal the parent that an exception has occurred
			System.out.println("[RoutingWorkItemHandler] Signalling that an exception has occurred");
			
			// Create the correlation key
			CorrelationKey key = JbpmUtil.createCorrelationKey(id);
			
			// Retrieve the process instance using the correlation key
			ProcessInstance pi = processService.getProcessInstance(key);
			if( pi == null ) {
				String msg = "[RoutingWorkItemHandler] Unable to retrieve process "
						+ "instance using '"+id+"' as the correlation key";
				System.out.println(msg);
				throw new IllegalStateException(msg);
			}
			
			// Send the signal
			processService.signalProcessInstance(pi.getId(), ROUTING_EXCEPTION, null);
			
			System.out.println("[RoutingWorkItemHandler] Signal sent to Process ID:"+pi.getId()+", KEY: "+id);
			
		} else
			manager.completeWorkItem(workItem.getId(), null);	
	}
	
}
