package ss.bpm.test;

import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.ProcessService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringBridge implements ApplicationContextAware, 
									 SpringBridgedServices {

	private static ApplicationContext appContext;
	
	@Autowired
	private ProcessService processService;
	
	@Autowired
	private DeploymentService deploymentService;
	
	@SuppressWarnings("static-access")
	@Override
	public void setApplicationContext(ApplicationContext appContext) 
			throws BeansException {
		this.appContext = appContext;
	}

	public static SpringBridgedServices services() {
		return appContext.getBean(SpringBridgedServices.class);
	}

	@Override
	public DeploymentService getDeploymentService() {
		return deploymentService;
	}

	@Override
	public ProcessService getProcessService() {
		return processService;
	}

	public void setProcessService(ProcessService processService) {
		this.processService = processService;
	}

	public void setDeploymentService(DeploymentService deploymentService) {
		this.deploymentService = deploymentService;
	}

}
