package ss.bpm.test;

import org.jbpm.kie.services.impl.KModuleDeploymentUnit;
import org.jbpm.services.api.model.DeploymentUnit;
import org.kie.internal.KieInternalServices;
import org.kie.internal.process.CorrelationKey;
import org.kie.internal.process.CorrelationKeyFactory;

public class JbpmUtil {
	
	private static final String G = "ss.bpm.test";
	private static final String A = "kjar";
	private static final String V = "1.0.0-SNAPSHOT";
	
	public static final DeploymentUnit defaultDeploymentUnit = new KModuleDeploymentUnit(G, A, V);
	
	public static CorrelationKey createCorrelationKey(String key) {
		CorrelationKeyFactory factory = KieInternalServices.Factory.get()
				.newCorrelationKeyFactory();
		CorrelationKey result = factory.newCorrelationKey(key);
		return result;
	}
}
