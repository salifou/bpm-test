package ss.bpm.test;

import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.ProcessService;

public interface SpringBridgedServices {
	
	public DeploymentService getDeploymentService();
	
	public ProcessService getProcessService(); 
}
