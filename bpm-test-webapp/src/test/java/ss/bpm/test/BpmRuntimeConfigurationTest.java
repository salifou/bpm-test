package ss.bpm.test;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.services.api.model.DeploymentUnit;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.process.CorrelationKey;

public class BpmRuntimeConfigurationTest extends AbstractBpmRuntimeTest {

	private static final DeploymentUnit defaulDeploymentUnit = JbpmUtil.defaultDeploymentUnit;
	
	/*@Test
	public void shouldCreateBpmServices(){
		Assert.assertNotNull( deploymentService );
		Assert.assertNotNull( userTaskService );
		Assert.assertNotNull( processService );
		Assert.assertNotNull( runtimeDataService );
	}*/
	
	@Test
	public void mainProcessWithouSignalTest() {
		
		String ID = "1";
		
		Map<String, Object> params = new HashMap<>();
		params.put("ID", ID);
		
		String processDefId = "main";
		
		ProcessInstance pi = startProcess(processDefId, ID, params);
		
		Assert.assertNotNull(pi);
	}
	
	/*@Test
	@Ignore
	public void mainProcessWithSubProcessSendingSignalTest() {
		
		String ID = "1";
		String THROW = "NO";
		
		Map<String, Object> params = new HashMap<>();
		params.put("ID", ID);
		params.put("THROW", THROW);
		
		String processDefId = "main";
		
		ProcessInstance pi = startProcess(processDefId, ID, params);
		
		Assert.assertNotNull(pi);
	}*/
	
	private ProcessInstance startProcess(String pdid, String key, Map<String, Object> params) {
		
		deploymentService.deploy(defaulDeploymentUnit);
		
		CorrelationKey ckey = JbpmUtil.createCorrelationKey(key);
		String depUnit = JbpmUtil.defaultDeploymentUnit.getIdentifier();
		long pid = processService.startProcess(depUnit, pdid, ckey, params);
		ProcessInstance pi = processService.getProcessInstance(pid);
		return pi;
	}
	
}
